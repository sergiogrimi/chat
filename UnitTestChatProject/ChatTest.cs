﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Models;

namespace UnitTestChatProject
{
    [TestClass]
    public class ChatTest
    {
        [TestMethod]
        public void Test_MessageNoText()
        {
            var expected = MessageType.Invalid;

            string msg = "";
            string sender = "pepe";
            string comm = "/stock";

            var result = Models.MessageParser.GetMessageDto(msg, sender, comm);
            Assert.AreEqual(expected, result.MessageType);
        }
        
        [TestMethod]
        public void Test_MessageCommand()
        {
            var expected = MessageType.Command;

            string msg = "/stock=aapl.us";
            string sender = "pepe";
            string comm = "/stock";

            var result = Models.MessageParser.GetMessageDto(msg, sender, comm);
            Assert.AreEqual(expected, result.MessageType);
        }
        
        [TestMethod]
        public void Test_MessageChat()
        {
            var expected = MessageType.Chat;

            string msg = "Hola Mundo";
            string sender = "pepe";
            string comm = "/stock";

            var result = Models.MessageParser.GetMessageDto(msg, sender, comm);
            Assert.AreEqual(expected, result.MessageType);
        }
    }
}
