﻿using Microsoft.AspNet.SignalR.Client;
using RabbitMQ.Client;
using RabbitMQ.Client.Events;
using System;
using System.IO;
using System.Net;
using System.Text;

namespace StockBot
{
    class Program
    {
        static IHubProxy myHub;
        static readonly string stockUrl = "https://stooq.com/q/l/?s=";
        static readonly string stockParameters = "&f=sd2t2ohlcv&h&e=csv";

        public static string GetStockInfo(string symbol) 
        {
            string url = $"{stockUrl}{symbol.ToLower()}{stockParameters}";

            HttpWebRequest req = (HttpWebRequest)WebRequest.Create(url);
            HttpWebResponse resp = (HttpWebResponse)req.GetResponse();

            StreamReader sr = new StreamReader(resp.GetResponseStream());
            string results = sr.ReadToEnd();

            var ret = results.Split(',');

            sr.Close();

            return ret[13];
        }

        public static void SendMessageToAllClients(IHubProxy p, string m) 
        {
            p.Invoke("BroadCastMessageNoUser", m);
        }

        public static IHubProxy ConnectToHub() 
        {
            var connection = new HubConnection("https://localhost:44378");
            IHubProxy myHub = connection.CreateHubProxy("ChatHub");

            connection.Start().Wait();

            return myHub;
        }
      
        static void Main(string[] args)
        {
            myHub = ConnectToHub();

            var factory = new ConnectionFactory() { HostName = "localhost" };
            using (var connection = factory.CreateConnection())
            using (var channel = connection.CreateModel())
            {
                channel.QueueDeclare(queue: "stocks", durable: false, exclusive: false, autoDelete: false, arguments: null);

                Console.WriteLine(" [*] Waiting for messages.");

                var consumer = new EventingBasicConsumer(channel);
                consumer.Received += (model, ea) =>
                {
                    var body = ea.Body;
                    var message = Encoding.UTF8.GetString(body);
                    Console.WriteLine(" [x] Info request  for {0} stock", message);
                    string priceRet = GetStockInfo(message);
                    SendMessageToAllClients(myHub, $"{message.ToUpper()} quote is ${priceRet} per share");
                };
                channel.BasicConsume(queue: "stocks", autoAck: true, consumer: consumer);

                Console.WriteLine(" Press [enter] to exit.");
                Console.ReadLine();
            }

        }
    }

}
