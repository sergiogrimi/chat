﻿using ChatRoomNetFwk.Models;
using Data;
using Microsoft.AspNet.SignalR;
using Models;
using RabbitMQ.Client;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Text;
using System.Threading.Tasks;
using CommomModel = Models;

namespace ChatRoomNetFwk
{
    public class ChatHub : Hub
    {
        Repo repo = new Repo();
        readonly int _maxChatMesages = Convert.ToInt32(ConfigurationManager.AppSettings["MaxChatMesages"] ?? "10");
        readonly bool _loadPreviousChats = Convert.ToBoolean(ConfigurationManager.AppSettings["LoadPreviousChats"] ?? "true");        
        readonly string _stockCommand = ConfigurationManager.AppSettings["StockCommand"] ?? "/stock";
        readonly string _chatBotName = ConfigurationManager.AppSettings["ChatBotName"] ?? "PepeBot";
        readonly string _stockPriceServiceUrl = ConfigurationManager.AppSettings["StockPriceServiceUrl"] ?? "PepeBot";
        readonly string _welcomeGreeting = ConfigurationManager.AppSettings["WelcomeGreeting"] ?? "Welcome!";
        readonly string _rabbitMqQueue = ConfigurationManager.AppSettings["RabbitMqQueue"] ?? "stocks";
        readonly string _rabbitMqServer = ConfigurationManager.AppSettings["RabbitMqServer"] ?? "localhost";
        readonly bool _storeChats = Convert.ToBoolean(ConfigurationManager.AppSettings["StoreChats"] ?? "true");
        
        
        public void Send(string name, string message)
        {   
            CommomModel.GenericStockMessage messageDto = MessageParser.GetMessageDto(message, name, _stockCommand);
            ProcessMessage(messageDto);
        }

        void StoreMessage(CommomModel.GenericStockMessage m) 
        {
            try
            {
                CommomModel.Message message = new CommomModel.Message();

                message.Text = m.Message;
                message.User = m.Sender;

                repo.Add(message);
            }
            catch (Exception ex)
            {

                throw ex;
            }
            
        }
        void ProcessMessage(CommomModel.GenericStockMessage m) 
        {
            switch (m.MessageType)
            {
                case CommomModel.MessageType.Chat:
                    BroadCastMessage(m.Sender, m.Message);
                    if (_storeChats) 
                    {
                        StoreMessage(m);
                    }
                    break;
                case CommomModel.MessageType.Command:
                    SendStockToBot(m.StockSymbol);
                    break;
                case CommomModel.MessageType.Invalid:
                    break;
                default:
                    break;
            }
        }
        void BroadCastMessage(string name, string message) 
        {
            Clients.All.addNewMessageToPage($"{DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss")} - {name}", message);
        }

        public void SendStockToBot(string message) 
        {
            var factory = new ConnectionFactory() { HostName = _rabbitMqServer };
            using (var connection = factory.CreateConnection())
            using (var channel = connection.CreateModel())
            {
                channel.QueueDeclare(queue: _rabbitMqQueue, durable: false, exclusive: false, autoDelete: false, arguments: null);

                var body = Encoding.UTF8.GetBytes(message);

                channel.BasicPublish(exchange: "", routingKey: _rabbitMqQueue, basicProperties: null, body: body);
            }
        }
        public void BroadCastMessageNoUser(string message)
        {
            Clients.All.addNewMessageToPage($"{DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss")} - {_chatBotName}", message);
        }

        public override Task OnConnected()
        {
            
            Clients.Client(Context.ConnectionId).addNewMessageToPage(_chatBotName, _welcomeGreeting);
            try
            {
                if (_loadPreviousChats)
                {
                    List<CommomModel.Message> mList = repo.Get(_maxChatMesages);

                    foreach (CommomModel.Message m in mList)
                    {
                        Clients.Client(Context.ConnectionId).addNewMessageToPage($"{m.MessageDate.ToString("yyyy-MM-dd HH:mm:ss")} - {m.User}", m.Text);
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            
            return base.OnConnected();
        }
    }
}