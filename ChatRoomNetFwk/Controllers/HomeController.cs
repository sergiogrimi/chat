﻿using Microsoft.AspNet.Identity;
using System;
using System.Configuration;
using System.Web.Mvc;

namespace ChatRoomNetFwk.Controllers
{
    public class HomeController : Controller
    {
        readonly int _maxChatMesages = Convert.ToInt32(ConfigurationManager.AppSettings["MaxChatMesages"] ?? "10");

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
        public ActionResult Chat()
        {
            ViewBag.MaxMessages = _maxChatMesages;
            ViewBag.User = User.Identity.GetUserName().Split('@')[0].ToString();
            return View();
        }
    }
}