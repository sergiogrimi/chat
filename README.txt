Resume:
=======

Solution Items:
===============

ChatDatabase: DB schema for the Chat DB used to store chats.
ChatRoomNetFwk: MVC app (net Fwk 4.7) contains chat screen and chat logic.
ChatsEntities: DB access with Entity Fwk, to store and get chat messages.
Data: Interface for accesing DB betwheen EF (ChatEntities) & MVC app.
Interaces: Interfaces definition.
Models: Models definition & Message Parser.
StockBot: Console app that get stock prices from web service and broadcast stock price to all connected chat users.
UnitTestChatProject: Unit Test for Message Parser.

STEPS:
======

1 - Create DB for chat msg storage and modify connectionstrings in "ChatEntities" & "ChatRoomNetFwk".
2 - Install RabbitMQ server: https://www.rabbitmq.com/download.html
3 - Copy Roslyn folder (attached in mail) to ChatRoomNetFwk\bin.
4 - Open the sln in VS2019 and rebuild (it will launch the MVC site).
5 - Register a user with email and passwd.
6 - Copy the url and open a new InPrivate browser window and register another user.
7 - Now both users can start chating clicking the "Financial Chat Room" link.
8 - Select the "StockBot" project and start a new debug instance of that project to allow connected users to consult stock prices.

General Architecture description
================================

- SIGNALR is used in MVC project to handle chat messages. (https://docs.microsoft.com/en-us/aspnet/signalr/overview/getting-started/tutorial-getting-started-with-signalr-and-mvc)
- RabbitMQ is used to inform stock prices request to the "SockBot" console app. A RabbitMQ client is installed in both projects.
- When a user types the command "/stock=appl.us" the MVC app post the stock symbol to the RabbitMQ server (channel "stock"), as the
"StockBot" is subscribed to that channel it will receive the new message with the stock symbol and will do a web request to ask for the
stock price to the URL given. When the web response is finished a "fast and dirty" parse will return the "Closed price" for the stock to
all connected users using the SIGNALR Hub reference.


Optional Features:
==================
- Use .NET identity for users authentication (OK)
- Handle messages that are not understood or any exceptions raised within the bot.(Partial understood messages handled)