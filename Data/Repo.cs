﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Data
{
    using ChatsEntities;
    using Interfaces;
    using Models;

    public class Repo : IData<Message>
    {
        public void Add(Message m)
        {
            using (var db = new ChatsEntities()) 
            {
                ChatMessage cm = new ChatMessage();

                cm.Message = m.Text;
                cm.PostedTime = DateTime.Now;
                cm.UserName = m.User;

                db.ChatMessages.Add(cm);
                db.SaveChanges();
            }
        }

        public List<Message> Get(int size)
        {
            List<Message> messageList = new List<Message>();

            using (var db = new ChatsEntities())
            {
                messageList = db.ChatMessages.Select(m => new Message
                                                        {
                                                            Id = m.Id,
                                                            MessageDate = m.PostedTime,
                                                            Text = m.Message,
                                                            User = m.UserName
                                                        })
                                .OrderByDescending(d => d.MessageDate).Take(size).ToList();    
            }

            return Enumerable.Reverse(messageList).ToList();
        }
    }
}
