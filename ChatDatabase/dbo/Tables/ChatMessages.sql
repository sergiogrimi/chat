﻿CREATE TABLE [dbo].[ChatMessages] (
    [Id]         INT            IDENTITY (1, 1) NOT NULL,
    [Message]    VARCHAR (1024) NOT NULL,
    [UserName]   VARCHAR (250)  NOT NULL,
    [PostedTime] DATETIME       NOT NULL,
    CONSTRAINT [PK_ChatMessages] PRIMARY KEY CLUSTERED ([Id] ASC)
);

