﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Models;

namespace Models
{
    public static class MessageParser
    {
        public static GenericStockMessage GetMessageDto(string m, string name, string stockCommand)
        {
            var subSt = m.Split('=');
            GenericStockMessage gm = new GenericStockMessage();
            gm.MessageType = MessageType.Invalid;
            gm.Sender = name;

            if (subSt[0].ToLower().Equals(stockCommand))
            {
                gm.MessageType = MessageType.Command;
                gm.Command = subSt[0];
                gm.StockSymbol = subSt[1];
            }
            else if (m.Length < 1000 && m.Length > 0 && name.Length > 0)
            {
                gm.Message = m;
                gm.MessageType = MessageType.Chat;
            }

            return gm;
        }
    }
}
