﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Models
{
    public enum MessageType 
    {   
        Chat = 0,
        Command = 1,
        Invalid = 2
    }
    public class Message
    {
        public int Id { get; set; }
        public string User { get; set; }
        public string Text { get; set; }
        public DateTime MessageDate { get; set; }
    }
}
