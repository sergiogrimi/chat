﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Models;

namespace Models
{
    
    public class GenericStockMessage
    {
        public MessageType MessageType { get; set; }
        public string Command { get; set; }
        public string StockSymbol { get; set; }
        public string Message { get; set; }
        public string Sender { get; set; }

    }
}